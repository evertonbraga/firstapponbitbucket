public class Fatorial {

	public int calcFatorial(int n) {
		if(n <= 1) {
			return 1;
		} else {
			return calcFatorial(n - 1) * n;
		}
	}
	
	public static void main(String[] args) {
		
		Fatorial f = new Fatorial();
		int fat = f.calcFatorial(7);
		System.out.println(fat);

	}
	
}
